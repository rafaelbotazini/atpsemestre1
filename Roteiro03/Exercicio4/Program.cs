﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Calculadora de aumento de salário (25%).");

            Console.Write("Digite o salário do funcionário: ");

            double aumento = double.TryParse(Console.ReadLine(), out double result) ? result : 0;

            aumento += aumento * 0.25;

            Console.WriteLine("O salário será aumentado para: {0:C}", aumento);

            Console.ReadKey(true);
        }
    }
}
