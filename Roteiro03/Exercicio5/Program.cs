﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio5
{
    class Program
    {
        static void Main()
        {
            try
            {
                Console.WriteLine("Calculadora de raízes da função de 2º grau");

                Console.WriteLine("Insira os termos da equação (a, b e c). ");
                Console.Write("a: ");
                double a = double.Parse(Console.ReadLine());

                Console.Write("b: ");
                double b = double.Parse(Console.ReadLine());

                Console.Write("c: ");
                double c = double.Parse(Console.ReadLine());
                
                if (a == 0)
                {
                    throw new Exception("Não é uma equação de 2º grau.");
                }

                double delta = b * b - 4 * a * c;

                if (delta < 0)
                {
                    throw new Exception("O delta é negativo");
                }

                double raizDelta = Math.Sqrt(delta);

                double x1 = -b + raizDelta / (2 * a);

                if (raizDelta == 0)
                {
                    Console.WriteLine("A equação tem uma unica raíz: {0:F2}.", x1);
                }
                else
                {
                    double x2 = -b - raizDelta / (2 * a);

                    Console.WriteLine("As raízes da equação são {0:F2} e {1:F2}", x1, x2);
                }

                Console.ReadKey(true);
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
                Console.ReadKey(true);
                Main();
            }
        }

        static void Start()
        {


        }
    }
}
