﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            double nota1 = 0;
            double nota2 = 0;
            double nota3 = 0;
           
            double result;

            Console.WriteLine("Calculadora de média aritmética.");
            Console.Write("Digite a primeira nota: ");

            if (double.TryParse(Console.ReadLine(), out result))
            {
                nota1 = result;
            }

            Console.Write("Digite a segunda nota: ");

            if(double.TryParse(Console.ReadLine(), out result))
            {
                nota2 = result;
            }

            Console.Write("Digite a terceira nota: ");

            if(double.TryParse(Console.ReadLine(), out result))
            {
                nota3 = result;
            }

            Console.WriteLine("A nota média é: {0:F2}", (nota1 + nota2 + nota3) / 3);
            Console.ReadKey();


        }
        
    }
}
