﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void txtCelsius_LostFocus(object sender, EventArgs e)
        {
            txtFarenheit.Text = string.Format("{0:F2}", ConverteFarenheit(txtCelsius.Text));
        }

        private void txtFarenheit_LostFocus(object sender, EventArgs e)
        {
            txtCelsius.Text = string.Format("{0:F2}", ConverteCelsius(txtFarenheit.Text));
        }

        static double ConverteCelsius(string s)
        {
            double.TryParse(s, out double result);

            double f = result;

            return (f - 32) / 1.8;
        }

        static double ConverteFarenheit(string s)
        {
            double.TryParse(s, out double result);

            double c = result;

            return (9 * c + 160) / 5;
        }
    }
}
