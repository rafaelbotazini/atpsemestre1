﻿using System;

namespace Exercicio2
{
    class Program
    {
        static void Main(string[] args)
        {

            double nota1 = 0;
            double nota2 = 0;
            double nota3 = 0;

            double peso1 = 0;
            double peso2 = 0;
            double peso3 = 0;

            double result;
            
            Console.WriteLine("Calculadora de média ponderada.");
            Console.Write("Digite a primeira nota: ");

            if (double.TryParse(Console.ReadLine(), out result))
            {
                nota1 = result;
            }

            Console.Write("Digite o peso da primeira nota: ");

            if (double.TryParse(Console.ReadLine(), out result))
            {
                peso1 = result;
            }

            Console.Write("Digite a segunda nota: ");

            if (double.TryParse(Console.ReadLine(), out result))
            {
                nota2 = result;
            }

            Console.Write("Digite o peso da segunda nota: ");

            if (double.TryParse(Console.ReadLine(), out result))
            {
                peso2 = result;
            }

            Console.Write("Digite a terceira nota: ");

            if (double.TryParse(Console.ReadLine(), out result))
            {
                nota3 = result;
            }

            Console.Write("Digite o peso da terceira nota: ");

            if (double.TryParse(Console.ReadLine(), out result))
            {
                peso3 = result;
            }

            nota1 *= peso1;
            nota2 *= peso2;
            nota3 *= peso3;

            nota1 += nota2 += nota3;
            peso1 += peso2 += peso3;

            Console.WriteLine("A média ponderada é: {0:F2}", nota1/peso1);
            Console.ReadKey();
        }
    }
}
