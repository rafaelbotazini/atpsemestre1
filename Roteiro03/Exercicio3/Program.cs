﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, d, x;

            Console.Write("\nExemplo 3 – Programa em C# ");
            Console.Write("\nEntre com um número de 4 algarismos e tecle <enter>: ");

            x = int.Parse(Console.ReadLine());

            a = x / 1000;
            b = (x % 1000) / 100;
            c = ((x % 1000) % 100) / 10;
            d = ((x % 1000) % 100) % 10;

            Console.Write("\n\nSaída:" + d + c + b + a);
            Console.Write("\nPressione uma tecla para terminar...");

            Console.ReadKey(false);
        }
    }
}
