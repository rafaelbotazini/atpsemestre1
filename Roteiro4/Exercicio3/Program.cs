﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Verificador de número par ou ímpar.");
            Console.Write("Digite um número inteiro: ");

            int numero = int.Parse(Console.ReadLine());

            string resultado = numero % 2 == 0 ? "par" : "ímpar";

            Console.WriteLine($"O número {numero} é {resultado}!");

            Console.WriteLine("Pressione uma tecla para sair.");
            Console.ReadKey(true);
        }
    }
}
