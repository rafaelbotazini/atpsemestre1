﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();

            Console.WriteLine("\tCalculador de tempo\n");
            Console.Write("Digite um número de dias: ");

            int dias = int.Parse(Console.ReadLine());

            int horas = dias * 24;

            int minutos = horas * 60;

            int segundos = dias * 60;

            Console.WriteLine($"{dias} dia(s) equivale(m) a {horas} horas, ou {minutos} minutos ou {segundos} segundos.");

            Console.WriteLine("Pressione uma tecla para sair.");
            Console.ReadKey(true);
        }
    }
}
