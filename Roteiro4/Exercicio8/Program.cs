﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio8
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("\tCalculadora de raízes da função do 2º grau\n");

            Console.WriteLine("Insira os três termos da equação ax² + bx + c.");

            Console.Write("a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("b = ");
            int b = int.Parse(Console.ReadLine());
            Console.Write("c = ");
            int c = int.Parse(Console.ReadLine());

            double delta;
            double raizDelta;
            double x1;
            double x2;

            string resultado = string.Empty;

            try
            {
                if (a == 0)                
                    throw new Exception("O primeiro termo é 0. Não é uma equação do 2º grau.");
                
                
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
                Console.ReadKey(true);
                Main();
            }

            
            
            

        }
    }
}
