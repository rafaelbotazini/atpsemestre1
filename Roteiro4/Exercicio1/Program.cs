﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            float numero1, // primeiro número a ser comparado
                numero2; // segundo número a ser comparado

            Console.Write("\nPor favor, digite o primeiro número real: ");
            numero1 = float.Parse(Console.ReadLine());

            Console.Write("\nPor favor, digite o segundo número real: ");
            numero2 = float.Parse(Console.ReadLine());

            // Compara os valores digitados pelo usuário
            if (numero1 == numero2)
                Console.WriteLine("\n" + numero1 + " é igual a " + numero2);

            if (numero1 != numero2)
                Console.WriteLine(numero1 + " é diferente " + numero2);

            if (numero1 < numero2)
                Console.WriteLine(numero1 + " é menor que " + numero2);

            if (numero1 > numero2)
                Console.WriteLine(numero1 + " é maior que " + numero2);

            if (numero1 <= numero2)
                Console.WriteLine(numero1 + " é menor ou igual a " + numero2);

            if (numero1 >= numero2)
                Console.WriteLine(numero1 + " é maior ou igual a " + numero2);

            Console.Write("\nPressione uma tecla para terminar...");
            Console.ReadKey(false);
        }
    }
}
