﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio6
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("\tOrdenador de números\n");
            Console.WriteLine("Insira três números a, b e c:\n");

            Console.Write("a = ");
            int a = getInt();

            Console.Write("b = ");
            int b = getInt();

            Console.Write("c = ");
            int c = getInt();
            

            string resultado = string.Empty;

            if (a == b && a == c)
            {
                resultado = $"Os valores são iguais.";
            }
            else if (a == b)
            {
                resultado = $"a e b têm o mesmo valor.";

                if (a < c)
                    resultado += $"\nA ordem é {a}, {b}, {c}.";
                else
                    resultado += $"\nA ordem é {c}, {a}, {b}.";
            }
            else if (a == c)
            {
                resultado = $"a e c têm o mesmo valor.";

                if (a < b)
                    resultado += $"\nA ordem é {a}, {c}, {b}.";
                else
                    resultado += $"\nA ordem é {b}, {c}, {a}.";
            }
            else if (b == c)
            {
                resultado = $"b e c têm o mesmo valor.";

                if (b < a)
                    resultado += $"\nA ordem é {b}, {c}, {a}.";
                else
                    resultado += $"\nA ordem é {a}, {b}, {c}.";
            }
            else
            {
                int maior = 0;
                int menor = 0;

                //pega o maior
                if (a > b && a > c)
                    maior = a;
                else if (b > a && b > c)
                    maior = b;
                else
                    maior = c;

                //pega o menor
                if (a < b && a < c)
                    menor = a;
                else if (b < a && b < c)
                    menor = b;
                else
                    menor = c;

                //pega o intermediário
                if (a < maior && a > menor)
                    resultado = $"A ordem é {menor}, {a}, {maior}.";
                else if (b < maior && b > menor)
                    resultado = $"A ordem é {menor}, {b}, {maior}.";
                else if (c < maior && c > menor)
                    resultado = $"A ordem é {menor}, {c}, {maior}.";

            }

            Console.WriteLine(resultado);

            Console.WriteLine("\nPressione uma tecla para sair.");
            Console.ReadKey(true);
            Console.Clear();
            Main();

        }

        static int getInt()
        {
            int n = int.Parse(Console.ReadLine());
            return n;
        }
    }
}
