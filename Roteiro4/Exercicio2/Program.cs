﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            int x;
            //Apresentação do Programa para o usuário
            Console.Write("\t***** PROGRAMA PARA TESTAR VALORES ******");

            //Entrada de Dados
            Console.Write("\n\nEntre com um valor inteiro qualquer: ");

            x = int.Parse(Console.ReadLine());
            //Processamento
            if (x == 0)
            {
                Console.Write("\n\nO valor digitado foi ZERO !!!");
            }
            else
            {
                if (x == 1)
                {
                    Console.Write("\n\nO valor digitado foi UM !!!");
                }
                else
                {
                    if (x == -1)
                    {
                        Console.Write("\n\nO valor digitado foi MENOS UM !!!");
                    }
                    else
                    {
                        Console.Write("\n\nFoi digitado outro valor diferente de Zero, Um ou Menos Um !!!");
                    }
                }
            }
            //Finalização do Programa
            Console.Write("\n\nTecle algo para terminar o programa. ");
            Console.ReadKey(true);
        }
    }
}
