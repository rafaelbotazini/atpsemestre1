﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\tVerificador de número.\n");

            Console.Write("Digite um número inteiro: ");

            int num = int.Parse(Console.ReadLine());

            string resultado = string.Empty;

            if (num > 0)
                resultado = "positivo";
            else if (num < 0)
                resultado = "negativo";
            else
                resultado = "nulo";

            Console.WriteLine($"O número {num} é {resultado}");
            Console.WriteLine("Pressione uma tecla para sair.");
            Console.ReadKey(true);
        }
    }
}
