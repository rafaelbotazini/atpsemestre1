﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio7
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("\tValidação de medidas de um triângulo\n");
            Console.WriteLine("Insira três valores inteiros e positivos a, b e c para serem as medidas dos lados do triângulo.");

            Console.Write("a = ");
            int a = int.Parse(Console.ReadLine());

            Console.Write("b = ");
            int b = int.Parse(Console.ReadLine());

            Console.Write("c = ");
            int c = int.Parse(Console.ReadLine());

            string resultado = string.Empty;

            if (a > b + c)
                resultado = "O valor do lado a é maior que a soma dos outros dois lados.";
            else if (b > a + c)
                resultado = "O valor do lado b é maior que a soma dos outros dois lados.";
            else if (c > a + b)
                resultado = "O valor do lado c é maior que a soma dos outros dois lados.";
            else if (a < 0 || b < 0 || c < 0)
                resultado = "As medidas dos lados devem ser dadas em números positivos.";
            else
            {
                if (a == b && a == c)
                    resultado = "O triângulo é equilátero.";
                else if (a == b || a == c || b == c)
                    resultado = "O triângulo é isósceles.";
                else
                    resultado = "O triânngulo é escaleno.";
            }

            Console.WriteLine(resultado);
            Console.ReadKey(true);
            Console.Clear();
            Main();
      
        }
    }
}
